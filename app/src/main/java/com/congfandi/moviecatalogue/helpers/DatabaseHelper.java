/*
 * Movie Catalogue
 * DatabaseHelper.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.helpers;


import com.congfandi.moviecatalogue.models.movie.Movie;
import com.congfandi.moviecatalogue.models.tv_show.TvShow;

import java.util.List;

import io.realm.Realm;

public class DatabaseHelper {
    private static Realm realm = Realm.getDefaultInstance();

    public static void saveMovie(Movie movie) {
        realm.beginTransaction();
        realm.copyToRealm(movie);
        realm.commitTransaction();
    }

    public static void closeConnection(){
        realm.close();
    }

    public static void deleteMovie(Movie movie) {
        final Movie myMovie = realm.where(Movie.class).equalTo("mId", movie.getId()).findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                assert myMovie != null;
                myMovie.deleteFromRealm();
            }
        });
    }

    public static boolean isMovieExist(Movie movie) {
        return !realm.where(Movie.class).equalTo("mId", movie.getId()).findAll().isEmpty();
    }

    public static List<Movie> getMovies() {
        return realm.where(Movie.class).findAll();
    }

    public static void saveTvShow(TvShow tvShow) {
        realm.beginTransaction();
        realm.copyToRealm(tvShow);
        realm.commitTransaction();
    }

    public static void deleteTvShow(TvShow tvShow) {
        final TvShow mTvShow = realm.where(TvShow.class).equalTo("mId", tvShow.getId()).findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                assert mTvShow != null;
                mTvShow.deleteFromRealm();
            }
        });
    }

    public static boolean isTvShowExist(TvShow tvShow) {
        return !realm.where(TvShow.class).equalTo("mId", tvShow.getId()).findAll().isEmpty();
    }

    public static List<TvShow> getTvShows() {
        return realm.where(TvShow.class).findAll();
    }

}
