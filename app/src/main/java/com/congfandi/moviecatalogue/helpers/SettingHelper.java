/*
 * Movie Catalogue
 * SettingHelper.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingHelper {

    private static SharedPreferences preferences;

    public static void setLanguage(String language, Context context) {
        preferences = context.getSharedPreferences(Constanta.SETTING_HELPER, Context.MODE_PRIVATE);
        preferences.edit().putString(Constanta.LANGUAGE, language).apply();
    }

    public static String getLanguage(Context context) {
        preferences = context.getSharedPreferences(Constanta.SETTING_HELPER, Context.MODE_PRIVATE);
        return preferences.getString(Constanta.LANGUAGE, "id-ID");
    }

}
