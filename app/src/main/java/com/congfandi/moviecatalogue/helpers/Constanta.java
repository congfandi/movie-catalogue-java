/*
 * Movie Catalogue
 * Constanta.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.helpers;

public class Constanta {
     public static final String SETTING_HELPER = "movie_catalogue";
     public static final String LANGUAGE = "language";
     public static final String MOVIE = "movie";
     public static final String TV_SHOW = "tv_show";
}
