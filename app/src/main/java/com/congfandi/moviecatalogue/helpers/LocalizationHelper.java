/*
 * Movie Catalogue
 * LocalizationHelper.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.helpers;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class LocalizationHelper {

    public static void setUp(Context context) {
        String languageCode = SettingHelper.getLanguage(context).split("-")[0];
        Locale locale = new Locale(languageCode);
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
    }
}
