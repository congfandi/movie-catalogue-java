/*
 * Movie Catalogue
 * FavoritePagerAdapter.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.ui.movie.FavoriteMovieMovieFragment;
import com.congfandi.moviecatalogue.ui.tv_show.FavoriteTvShowFragment;

public class FavoritePagerAdapter extends FragmentPagerAdapter {
    private Context context;

    public FavoritePagerAdapter(Context context, @NonNull FragmentManager fm,int behavior) {
        super(fm,behavior);
        this.context = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        if (position == 0) fragment = new FavoriteMovieMovieFragment();
        else fragment = new FavoriteTvShowFragment();
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title;
        if (position == 0) title = context.getResources().getString(R.string.movie);
        else title = context.getResources().getString(R.string.tv_show);
        return title;
    }
}
