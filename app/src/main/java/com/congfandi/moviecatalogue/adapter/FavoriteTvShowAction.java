/*
 * Movie Catalogue
 * FavoriteTvShowAction.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.adapter;

import com.congfandi.moviecatalogue.models.tv_show.TvShow;

public interface FavoriteTvShowAction {
    void onDeleted(TvShow tvShow);
}
