/*
 * Movie Catalogue
 * TvShowAdapter.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.activities.DetailTvShowActivity;
import com.congfandi.moviecatalogue.helpers.Constanta;
import com.congfandi.moviecatalogue.helpers.DatabaseHelper;
import com.congfandi.moviecatalogue.models.tv_show.TvShow;

import java.util.ArrayList;
import java.util.List;

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.TvShowHolder> {
    private Context context;
    private List<TvShow> listMovie = new ArrayList<>();
    private FavoriteTvShowAction tvShowAction;

    public void updateData(List<TvShow> listMovie) {
        this.listMovie.clear();
        this.listMovie.addAll(listMovie);
        notifyDataSetChanged();
    }

    public void deleteTvShow(TvShow tvShow) {
        if (listMovie.contains(tvShow))
            listMovie.remove(tvShow);
        notifyDataSetChanged();
    }

    public TvShowAdapter(Context context, FavoriteTvShowAction tvShowAction) {
        this.context = context;
        this.tvShowAction = tvShowAction;
    }

    public TvShowAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public TvShowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TvShowHolder(LayoutInflater.from(context).inflate(R.layout.movie_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowHolder holder, int position) {
        holder.bindView(listMovie.get(position));
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    class TvShowHolder extends RecyclerView.ViewHolder {

        void bindView(final TvShow tvShow) {

            rating.setText(context.getResources().getString(R.string.rating, tvShow.getVoteAverage().toString()));
            name.setText(tvShow.getName());
            Glide.with(context).load(context.getResources().getString(R.string.poster_url, "w185", tvShow.getPosterPath())).into(poster);
            Glide.with(context).load(DatabaseHelper.isTvShowExist(tvShow) ? R.drawable.ic_favorite : R.drawable.ic_unfavorite).into(favorite);
            poster.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailTvShowActivity.class);
                    intent.putExtra(Constanta.MOVIE, tvShow);
                    context.startActivity(intent);
                }
            });
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (DatabaseHelper.isTvShowExist(tvShow)) {
                        Glide.with(context).load(R.drawable.ic_unfavorite).into(favorite);
                        DatabaseHelper.deleteTvShow(tvShow);
                        if (tvShowAction != null) tvShowAction.onDeleted(tvShow);
                    } else {
                        Glide.with(context).load(R.drawable.ic_favorite).into(favorite);
                        DatabaseHelper.saveTvShow(tvShow);
                    }
                }
            });
        }

        private ImageView poster, favorite;
        private TextView rating;
        private TextView name;

        public TvShowHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.img_poster_item);
            rating = itemView.findViewById(R.id.tv_detail_item);
            name = itemView.findViewById(R.id.tv_title_item);
            favorite = itemView.findViewById(R.id.btn_favorite_item);
        }
    }
}
