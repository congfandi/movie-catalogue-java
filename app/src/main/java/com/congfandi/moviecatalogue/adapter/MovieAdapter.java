/*
 * Movie Catalogue
 * MovieAdapter.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.activities.DetailMovieActivity;
import com.congfandi.moviecatalogue.helpers.Constanta;
import com.congfandi.moviecatalogue.helpers.DatabaseHelper;
import com.congfandi.moviecatalogue.models.movie.Movie;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {
    private Context context;
    private List<Movie> listMovie = new ArrayList<>();

    private FavoriteMovieAction favoriteAction;

    public MovieAdapter(Context context, FavoriteMovieAction favoriteAction) {
        this.context = context;
        this.favoriteAction = favoriteAction;
    }

    public void updateData(List<Movie> listMovie) {
        this.listMovie.clear();
        this.listMovie.addAll(listMovie);
        notifyDataSetChanged();
    }

    public void removeMovie(Movie movie) {
        if (listMovie.contains(movie))
            listMovie.remove(movie);
        notifyDataSetChanged();
    }

    public MovieAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MovieHolder(LayoutInflater.from(context).inflate(R.layout.movie_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        holder.bindView(listMovie.get(position));
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    class MovieHolder extends RecyclerView.ViewHolder {

        void bindView(final Movie movie) {

            rating.setText(context.getResources().getString(R.string.rating, movie.getVoteAverage().toString()));
            name.setText(movie.getTitle());
            Glide.with(context).load(context.getResources().getString(R.string.poster_url, "w185", movie.getPosterPath())).into(poster);
            Glide.with(context).load(
                    DatabaseHelper.isMovieExist(movie) ? R.drawable.ic_favorite : R.drawable.ic_unfavorite
            ).into(favorite);
            poster.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailMovieActivity.class);
                    intent.putExtra(Constanta.MOVIE, movie);
                    context.startActivity(intent);
                }
            });
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (DatabaseHelper.isMovieExist(movie)) {
                        DatabaseHelper.deleteMovie(movie);
                        Glide.with(context).load(R.drawable.ic_unfavorite).into(favorite);
                        if (favoriteAction != null) favoriteAction.onDeleted(movie);
                    } else {
                        DatabaseHelper.saveMovie(movie);
                        Glide.with(context).load(R.drawable.ic_favorite).into(favorite);
                    }
                }
            });


        }

        private ImageView poster, favorite;
        private TextView rating;
        private TextView name;

        public MovieHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.img_poster_item);
            rating = itemView.findViewById(R.id.tv_detail_item);
            name = itemView.findViewById(R.id.tv_title_item);
            favorite = itemView.findViewById(R.id.btn_favorite_item);
        }
    }
}

