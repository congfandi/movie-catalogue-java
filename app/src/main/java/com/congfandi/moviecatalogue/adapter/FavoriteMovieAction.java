/*
 * Movie Catalogue
 * FavoriteMovieAction.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.adapter;

import com.congfandi.moviecatalogue.models.movie.Movie;

public interface FavoriteMovieAction {
    void onDeleted(Movie movie);
}
