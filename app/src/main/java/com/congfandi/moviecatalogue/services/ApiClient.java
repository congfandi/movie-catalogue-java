/*
 * Movie Catalogue
 * ApiClient.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.services;

import com.congfandi.moviecatalogue.models.movie.MovieResponse;
import com.congfandi.moviecatalogue.models.tv_show.TvShowResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiClient {
    @GET("3/discover/movie")
    Call<MovieResponse> listMovie(@Query("api_key") String api_key, @Query("language") String language);

    @GET("3/discover/tv")
    Call<TvShowResponse> listTvShow(@Query("api_key") String api_key, @Query("language") String language);
}
