
package com.congfandi.moviecatalogue.models.tv_show;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TvShowResponse {

    @SerializedName("page")
    private Long mPage;
    @SerializedName("results")
    private List<TvShow> mTvShows;
    @SerializedName("total_pages")
    private Long mTotalPages;
    @SerializedName("total_results")
    private Long mTotalResults;

    public Long getPage() {
        return mPage;
    }

    public void setPage(Long page) {
        mPage = page;
    }

    public List<TvShow> tvShows() {
        return mTvShows;
    }

    public void setResults(List<TvShow> tvShows) {
        mTvShows = tvShows;
    }

    public Long getTotalPages() {
        return mTotalPages;
    }

    public void setTotalPages(Long totalPages) {
        mTotalPages = totalPages;
    }

    public Long getTotalResults() {
        return mTotalResults;
    }

    public void setTotalResults(Long totalResults) {
        mTotalResults = totalResults;
    }

}
