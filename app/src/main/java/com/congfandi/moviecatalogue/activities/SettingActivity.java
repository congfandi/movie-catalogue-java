/*
 * Movie Catalogue
 * SettingActivity.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.activities;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.helpers.LocalizationHelper;
import com.congfandi.moviecatalogue.helpers.SettingHelper;

public class SettingActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        init();
    }


    private void init() {
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        RadioButton radioBahasa = findViewById(R.id.radio_bahasa);
        RadioButton radioEnglish = findViewById(R.id.radio_englis);
        radioGroup.setOnCheckedChangeListener(this);
        radioBahasa.setChecked(SettingHelper.getLanguage(this).equals("id-ID"));
        radioEnglish.setChecked(SettingHelper.getLanguage(this).equals("en-US"));
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_bahasa:
                SettingHelper.setLanguage("id-ID", this);
                break;
            case R.id.radio_englis:
                SettingHelper.setLanguage("en-US", this);
                break;

        }
        LocalizationHelper.setUp(this);
    }
}
