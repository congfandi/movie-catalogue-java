/*
 * Movie Catalogue
 * SplashActivity.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.congfandi.moviecatalogue.BuildConfig;
import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.helpers.LocalizationHelper;

public class SplashActivity extends AppCompatActivity {

    private TextView version;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        version = findViewById(R.id.tv_version);
        version.setText(getResources().getString(R.string.version, BuildConfig.VERSION_NAME));
        LocalizationHelper.setUp(this);
        postDelay();
    }

    private void postDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 3000);
    }
}
