/*
 * Movie Catalogue
 * DetailMovieActivity.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.helpers.Constanta;
import com.congfandi.moviecatalogue.helpers.DatabaseHelper;
import com.congfandi.moviecatalogue.models.movie.Movie;


import io.realm.Realm;

public class DetailMovieActivity extends AppCompatActivity {
    private Movie movie;
    private Menu menu;

    private void init() {
        movie = getIntent().getParcelableExtra(Constanta.MOVIE);
        setUpToolbar();
        ImageView poster = findViewById(R.id.img_detail_tv_show);
        Glide.with(this).load(getResources().getString(R.string.poster_url, "w500", movie.getPosterPath())).into(poster);
        TextView rating = findViewById(R.id.tv_rating);
        rating.setText(getResources().getString(R.string.rating, movie.getVoteAverage().toString()));
        TextView release = findViewById(R.id.tv_release);
        release.setText(getResources().getString(R.string.release, movie.getReleaseDate()));
        TextView overview = findViewById(R.id.tv_detail_tv_show);
        overview.setText(movie.getOverview());
        getSupportActionBar().setTitle(movie.getTitle());
    }

    private void saveMovie() {
        if (!DatabaseHelper.isMovieExist(movie)) {
            DatabaseHelper.saveMovie(movie);
            menu.getItem(0).setIcon(R.drawable.ic_favorite);
        } else {
            DatabaseHelper.deleteMovie(movie);
            menu.getItem(0).setIcon(R.drawable.ic_unfavorite);
        }
    }


    private void setUpToolbar() {
        Toolbar myChildToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myChildToolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.favorite_menu, menu);
        menu.getItem(0).setIcon(
                !DatabaseHelper.isMovieExist(movie) ?
                        R.drawable.ic_unfavorite :
                        R.drawable.ic_favorite
        );
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            saveMovie();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tv_show);
        init();
        setUpToolbar();
    }
}
