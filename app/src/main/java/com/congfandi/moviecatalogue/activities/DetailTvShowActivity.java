/*
 * Movie Catalogue
 * DetailTvShowActivity.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.helpers.Constanta;
import com.congfandi.moviecatalogue.helpers.DatabaseHelper;
import com.congfandi.moviecatalogue.models.tv_show.TvShow;

public class DetailTvShowActivity extends AppCompatActivity {
    TvShow tvShow;
    private Menu menu;
    Toolbar toolbar;

    private void init() {
        tvShow = getIntent().getParcelableExtra(Constanta.MOVIE);
        ImageView poster = findViewById(R.id.img_detail_tv_show);
        Glide.with(this).load(getResources().getString(R.string.poster_url, "w500", tvShow.getPosterPath())).into(poster);
        TextView rating = findViewById(R.id.tv_rating);
        rating.setText(getResources().getString(R.string.rating, tvShow.getVoteAverage().toString()));
        TextView release = findViewById(R.id.tv_release);
        release.setText(getResources().getString(R.string.release, tvShow.getFirstAirDate()));
        TextView overview = findViewById(R.id.tv_detail_tv_show);
        overview.setText(tvShow.getOverview());
        setUpToolbar();
    }

    private void setUpToolbar() {
        Toolbar myChildToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myChildToolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

    }

    private void setTvShow() {
        if (!DatabaseHelper.isTvShowExist(tvShow)) {
            DatabaseHelper.saveTvShow(tvShow);
            menu.getItem(0).setIcon(R.drawable.ic_favorite);
        } else {
            DatabaseHelper.deleteTvShow(tvShow);
            menu.getItem(0).setIcon(R.drawable.ic_unfavorite);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.favorite_menu, menu);
        menu.getItem(0).setIcon(
                !DatabaseHelper.isTvShowExist(tvShow) ?
                        R.drawable.ic_unfavorite :
                        R.drawable.ic_favorite
        );
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                setTvShow();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tv_show);
        init();
    }
}
