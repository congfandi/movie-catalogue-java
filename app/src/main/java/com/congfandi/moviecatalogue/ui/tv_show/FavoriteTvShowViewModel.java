/*
 * Movie Catalogue
 * FavoriteTvShowViewModel.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.tv_show;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.congfandi.moviecatalogue.helpers.DatabaseHelper;
import com.congfandi.moviecatalogue.models.tv_show.TvShow;

import java.util.List;


public class FavoriteTvShowViewModel extends ViewModel {
    private MutableLiveData<List<TvShow>> listMovie = new MutableLiveData<>();

    public void requestTvShows() {
        listMovie.postValue(DatabaseHelper.getTvShows());
    }

    public LiveData<List<TvShow>> getTvShows() {
        return listMovie;
    }
}
