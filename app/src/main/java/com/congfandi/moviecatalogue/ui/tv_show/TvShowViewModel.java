/*
 * Movie Catalogue
 * TvShowViewModel.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.tv_show;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.congfandi.moviecatalogue.BuildConfig;
import com.congfandi.moviecatalogue.helpers.SettingHelper;
import com.congfandi.moviecatalogue.models.tv_show.TvShow;
import com.congfandi.moviecatalogue.models.tv_show.TvShowResponse;
import com.congfandi.moviecatalogue.services.ApiClient;
import com.congfandi.moviecatalogue.services.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowViewModel extends ViewModel {

    private MutableLiveData<List<TvShow>> listMovie = new MutableLiveData<>();

    public void requestTvShows(Context context) {
        ApiClient apiClient = ApiService.config().create(ApiClient.class);
        Call<TvShowResponse> request = apiClient.listTvShow(BuildConfig.API_KEY, SettingHelper.getLanguage(context));
        request.enqueue(new Callback<TvShowResponse>() {
            @Override
            public void onResponse(Call<TvShowResponse> call, Response<TvShowResponse> response) {
                listMovie.postValue(response.body().tvShows());
            }

            @Override
            public void onFailure(Call<TvShowResponse> call, Throwable t) {

            }
        });
    }

    public LiveData<List<TvShow>> getTvShows() {
        return listMovie;
    }
}