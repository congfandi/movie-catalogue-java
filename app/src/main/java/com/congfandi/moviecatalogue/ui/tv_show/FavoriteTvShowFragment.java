/*
 * Movie Catalogue
 * FavoriteTvShowFragment.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.tv_show;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.adapter.FavoriteTvShowAction;
import com.congfandi.moviecatalogue.adapter.TvShowAdapter;
import com.congfandi.moviecatalogue.models.tv_show.TvShow;

import java.util.List;

public class FavoriteTvShowFragment extends Fragment implements FavoriteTvShowAction {
    private FavoriteTvShowViewModel tvShowViewModel;
    private ProgressBar progressBar;
    private TvShowAdapter tvShowAdapter;
    private TextView noData;

    private void init(View view) {
        noData = view.findViewById(R.id.nodata);
        RecyclerView recyclerView = view.findViewById(R.id.rv_movie);
        progressBar = view.findViewById(R.id.progressBarMovie);
        progressBar.setVisibility(View.VISIBLE);
        tvShowAdapter = new TvShowAdapter(getContext(), this);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(tvShowAdapter);
    }

    private void fetchData() {
        tvShowViewModel.requestTvShows();
        tvShowViewModel.getTvShows().observe(this, new Observer<List<TvShow>>() {
            @Override
            public void onChanged(List<TvShow> movies) {
                progressBar.setVisibility(View.INVISIBLE);
                tvShowAdapter.updateData(movies);
                if (movies.isEmpty()) noData.setVisibility(View.VISIBLE);
                else noData.setVisibility(View.INVISIBLE);
            }
        });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        tvShowViewModel =
                ViewModelProviders.of(this).get(FavoriteTvShowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        init(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchData();
    }

    @Override
    public void onDeleted(TvShow tvShow) {
        fetchData();
    }
}
