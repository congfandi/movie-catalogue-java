/*
 * Movie Catalogue
 * MovieFragment.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.movie;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.adapter.MovieAdapter;
import com.congfandi.moviecatalogue.models.movie.Movie;

import java.util.List;

public class MovieFragment extends Fragment {

    private MovieViewModel homeViewModel;
    private ProgressBar progressBar;
    private MovieAdapter movieAdapter;

    private void init(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.rv_movie);
        progressBar = view.findViewById(R.id.progressBarMovie);
        progressBar.setVisibility(View.VISIBLE);
        movieAdapter = new MovieAdapter(getContext());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(movieAdapter);
    }

    private void fetchData() {
        homeViewModel.requestMovies(getContext());
        homeViewModel.getMovie().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> movies) {
                progressBar.setVisibility(View.INVISIBLE);
                movieAdapter.updateData(movies);
            }
        });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(MovieViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        init(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchData();
    }
}