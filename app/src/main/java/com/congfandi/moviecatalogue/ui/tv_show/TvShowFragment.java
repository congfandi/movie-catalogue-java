/*
 * Movie Catalogue
 * TvShowFragment.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.tv_show;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.adapter.TvShowAdapter;
import com.congfandi.moviecatalogue.models.tv_show.TvShow;

import java.util.List;

public class TvShowFragment extends Fragment {

    private TvShowViewModel tvShowViewModel;
    private ProgressBar progressBar;
    private TvShowAdapter movieAdapter;

    private void init(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.rv_movie);
        progressBar = view.findViewById(R.id.progressBarMovie);
        progressBar.setVisibility(View.VISIBLE);
        movieAdapter = new TvShowAdapter(getContext());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(movieAdapter);
    }

    private void fetchData() {
        tvShowViewModel.requestTvShows(getContext());
        tvShowViewModel.getTvShows().observe(this, new Observer<List<TvShow>>() {
            @Override
            public void onChanged(List<TvShow> movies) {
                progressBar.setVisibility(View.INVISIBLE);
                movieAdapter.updateData(movies);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchData();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        tvShowViewModel =
                ViewModelProviders.of(this).get(TvShowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        init(root);
        return root;
    }

}