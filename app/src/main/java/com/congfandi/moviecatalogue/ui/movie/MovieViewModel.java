/*
 * Movie Catalogue
 * MovieViewModel.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.movie;


import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.congfandi.moviecatalogue.BuildConfig;
import com.congfandi.moviecatalogue.helpers.SettingHelper;
import com.congfandi.moviecatalogue.models.movie.Movie;
import com.congfandi.moviecatalogue.models.movie.MovieResponse;
import com.congfandi.moviecatalogue.services.ApiClient;
import com.congfandi.moviecatalogue.services.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieViewModel extends ViewModel {

    private MutableLiveData<List<Movie>> listMovie = new MutableLiveData<>();

    public void requestMovies(Context context) {
        ApiClient apiClient = ApiService.config().create(ApiClient.class);
        Call<MovieResponse> request = apiClient.listMovie(BuildConfig.API_KEY, SettingHelper.getLanguage(context));
        request.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                listMovie.postValue(response.body().getMovies());
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }
        });
    }

    public LiveData<List<Movie>> getMovie() {
        return listMovie;
    }


}