/*
 * Movie Catalogue
 * FavoriteMovieViewModel.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.movie;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.congfandi.moviecatalogue.models.movie.Movie;


import java.util.List;

import io.realm.Realm;


public class FavoriteMovieViewModel extends ViewModel {
    private MutableLiveData<List<Movie>> listMovie = new MutableLiveData<>();
    private Realm realm = Realm.getDefaultInstance();

    public void requestMovies() {
        listMovie.postValue(realm.where(Movie.class).findAll());
    }

    public LiveData<List<Movie>> getMovie() {
        return listMovie;
    }

}
