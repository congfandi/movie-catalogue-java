/*
 * Movie Catalogue
 * FavoriteMovieMovieFragment.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.movie;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.adapter.FavoriteMovieAction;
import com.congfandi.moviecatalogue.adapter.MovieAdapter;
import com.congfandi.moviecatalogue.models.movie.Movie;

import java.util.List;

public class FavoriteMovieMovieFragment extends Fragment implements FavoriteMovieAction {
    private FavoriteMovieViewModel homeViewModel;
    private ProgressBar progressBar;
    private MovieAdapter movieAdapter;
    private TextView noData;

    private void init(View view) {
        noData = view.findViewById(R.id.nodata);
        RecyclerView recyclerView = view.findViewById(R.id.rv_movie);
        progressBar = view.findViewById(R.id.progressBarMovie);
        progressBar.setVisibility(View.VISIBLE);
        movieAdapter = new MovieAdapter(getContext(), this);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(movieAdapter);
    }

    private void fetchData() {
        homeViewModel.requestMovies();
        homeViewModel.getMovie().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> movies) {
                progressBar.setVisibility(View.INVISIBLE);
                movieAdapter.updateData(movies);
                if (movies.isEmpty()) noData.setVisibility(View.VISIBLE);
                else noData.setVisibility(View.INVISIBLE);
            }
        });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(FavoriteMovieViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        init(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchData();
    }

    @Override
    public void onDeleted(Movie movie) {
        fetchData();
    }
}
