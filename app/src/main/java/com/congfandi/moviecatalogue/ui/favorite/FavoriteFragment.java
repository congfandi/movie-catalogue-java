/*
 * Movie Catalogue
 * FavoriteFragment.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.favorite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.congfandi.moviecatalogue.R;
import com.congfandi.moviecatalogue.adapter.FavoritePagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class FavoriteFragment extends Fragment {
    private void init(View view) {
        TabLayout tabLayout = view.findViewById(R.id.tab_favorite);
        ViewPager viewPager = view.findViewById(R.id.viewpager_parent);
        viewPager.setAdapter(new FavoritePagerAdapter(getContext(), getChildFragmentManager(), 2));
        tabLayout.setupWithViewPager(viewPager);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.favorite_fragment, container, false);
        init(root);
        return root;
    }
}