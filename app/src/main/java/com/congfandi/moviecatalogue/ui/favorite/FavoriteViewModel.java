/*
 * Movie Catalogue
 * FavoriteViewModel.java
 * Created by Cong Fandi on 3/11/2019
 * Copyright © 2019 Cong Fandi. All rights reserved.
 *
 */

package com.congfandi.moviecatalogue.ui.favorite;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FavoriteViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public FavoriteViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is notifications fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}